# Py 2/3 compatibility:
try:
    # Py 2
    from scheduler import Job, jobs, start, parse, interval, threads, func_repo\
         as function
    from scheduler import set_cancel
    import datecalc
except ImportError:
    # Py 3
    from scheduler.scheduler import Job, jobs, start, parse, interval, threads,\
        func_repo as function
    from scheduler.scheduler import set_cancel
    import scheduler.datecalc
