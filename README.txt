1.       Проверять наличие изменений во всех ветках репозитория в заданный интервал времени

2.       При обнаружении изменений в ветке вытянуть изменения, собрать код.

3.       Запустить полученное приложение, сохранить вывод приложения в консоль.

4.       Запаковать приложение в nuget пакет. При этом в метаданных пакета должны быть:
   a.       ID пакета (любое фиксированное значение),
   b.       Version - (задание со звездочкой - версия, сформированная при сборке приложения из исходников)
   c.       Описание - в описании необходимо отобразить вывод приложения в консоль + хеш последнего коммита.
   d.       Автор - автор коммита.

 5.       Полученный нугет пакет необходимо выложить (руками или, в качестве задания со звездочкой - из скрипта) на www.nuget.org (другой любой нугет хостинг).

 6.       Дополнительно, как задание со звездочками: настройки, такие как URL репозитория, частота проверки, e-mail адрес для уведомлений, \
          необходимо передавать либо как аргументы скрипту, либо вычитывать из конфигурационного файла.
 
------------------------
Подготовка окружения
 
rpm -i https://packages.microsoft.com/config/rhel/7/packages-microsoft-prod.rpm
yum install dotnet-hosting-2.0.9 dotnet-sdk-2.1.202

yum install python-yaml python-pygit2

-----------------------
Запуск

python builder.py -c trytobuild.yml

Файл конфигурации в YAML

Для планировщика используется сторонняя библиотека (немного модифицированная)
https://github.com/ziberna/py-scheduler/blob/master/scheduler/

Формат распсания (schedule) - подобный cron, но с секундным полем (sec min hour weekday day month)
В примере расписание - раз в 15 минут

Имена пакетов получаются в формате ${NAME}-${RELEASE}-git${LAST_COMMIT_HASH}

Отправка уведомлений - по протоколу SMTP, поддерживается шифрование (TLS) и авторизация (установите поля user и password, если нужна)


repo:
  type: git
  url: https://gitlab.com/ms1980/trytobuild
  branch: master
schedule: '0 */15 * * * *'
# sec min hour weekday day month
build:
  repodir: /home/build/trytobuild
  vars:
    RELEASE: Release
    RELEASEVER: 1.0.0
    NAME: trytobuild-ms
    API_KEY: <API_KEY_NUGET> 
    API_URL: https://api.nuget.org/v3/index.json
  cmd: dotnet build -c ${RELEASE}
  run: dotnet run -c ${RELEASE}
  pkg: 'dotnet pack -c ${RELEASE} --version-suffix ${VERSION} /p:Authors="${AUTHOR}" /p:Description="${DESCRIPTION}" /p:PackageId=${NAME}'
  publish: dotnet nuget push bin/${RELEASE}/${NAME}.${VERSION}.nupkg -k ${API_KEY} -s ${API_URL}
  state: /home/build/trytobuild.state
  lock: /home/build/trytobuild.lock
  log: /home/build/trytobuild.log
  notify:
    type: smtp
    #tls: 1
    host: <SMTP_HOST>
    #user: <USERNAME>
    #password: <PASSWORD>
    from: <FROM_ADDR>
    to: <TO_ADDR>

