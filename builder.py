#!/usr/bin/python

import sys
import os
import signal
import fcntl
import re
import glob
import argparse
import yaml
import traceback
import time
import datetime
import pygit2
import subprocess
import logging
import smtplib
import scheduler


verbose = False

FORMAT = '%(asctime)-15s %(message)s'
logging.basicConfig(level=logging.INFO, format=FORMAT)

cfg = None

running = True


def proc_run(cmd, chdir, vars = None):
    if not vars is None:
        env = os.environ.copy()
        env.update(vars)
    else:
        env = None

    proc = subprocess.Popen("cd %s && %s" % (chdir, cmd), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, env=env)
    outstream = []
    errstream = []
    (out, err) = proc.communicate()
    for line in out:
        if line is None: break
        outstream.append(str(line))
    for line in err:
        if line is None: break
        errstream.append(str(line))

    return (proc.returncode, outstream, errstream)


class Commiter:
    def __init__(self, name, email):
        self.name = name
        self.email = email


class Commit:
    def __init__(self, commit, time, msg, commiter):
        self.commit = commit
        self.time = time
        self.msg = msg
        self.commiter = commiter

    def __str__(self):
        return "%s %s '%s' (%s) '%s'" % (self.commit, 
            datetime.datetime.fromtimestamp(self.time).strftime('%Y-%m-%d %H:%M:%S'), 
            self.commiter.name, self.commiter.email, self.msg)


class Repo:
    def __init__(self, url, branch = None):
        self.url = url
        self.branch = 'master' if branch is None else branch

    def __str__(self):
        return "{ url = %s, branch = %s }" % (self.url, self.branch)

    @staticmethod
    def get(repo, key):
        if repo is None:
            logging.error("%s key not found", key)
            raise ValueError()

        error = False
        url = repo.get('url')
        if url is None:
            logging.error("url in %s key not found", key)
            error = True
        else:
            if not url.startswith('https://'):
                logging.error("url protocol in %s key not supported: %s", key, url)
                error = True

        rtype = repo.get('type')
        if rtype is None:
            logging.error("type in %s key not found", key)
            error = True
        else:
            handler = repo_types.get(rtype)
            if handler is None:
                logging.error("type %s in %s key not supported", rtype, key)
                error = True

        if error:
            raise ValueError()

        return handler(url, repo.get('branch'))


class RepoGit(Repo):

    def load(self, repodir):
        if os.path.exists(repodir):
            if not os.path.isdir(repodir):
                raise RuntimeError("%s exists and is not a dir" % repodir)
        else:
            os.mkdir(repodir)

        gitdir = os.path.join(repodir, ".git")
        if os.path.exists(gitdir): # fetch
            self.repo = pygit2.Repository(gitdir)
            self.pull()
        else:
            self.repo = pygit2.clone_repository(self.url, repodir)

            if self.repo.head.shorthand != self.branch: # Checkout branch
                branch = self.repo.lookup_branch(self.branch)
                ref = self.repo.lookup_reference(branch.name)
                self.repo.checkout(ref)

    def pull(self, remote_name='origin'):
        for remote in self.repo.remotes:
            if remote.name == remote_name:
                remote.fetch()
                remote_master_id = self.repo.lookup_reference('refs/remotes/origin/%s' % (self.branch)).target
                merge_result, _ = self.repo.merge_analysis(remote_master_id)
                # Up to date, do nothing
                if merge_result & pygit2.GIT_MERGE_ANALYSIS_UP_TO_DATE:
                    return
                # We can just fastforward
                elif merge_result & pygit2.GIT_MERGE_ANALYSIS_FASTFORWARD:
                    self.repo.checkout_tree(self.repo.get(remote_master_id))
                    try:
                        master_ref = self.repo.lookup_reference('refs/heads/%s' % (self.branch))
                        master_ref.set_target(remote_master_id)
                    except KeyError:
                        self.repo.create_branch(branch, self.repo.get(remote_master_id))
                        
                    self.repo.head.set_target(remote_master_id)
                elif merge_result & pygit2.GIT_MERGE_ANALYSIS_NORMAL:
                    self.repo.merge(remote_master_id)

                    if self.repo.index.conflicts is not None:
                        for conflict in repo.index.conflicts:
                            logging.error('Conflicts found in: %s', conflict[0].path)
                        raise AssertionError('Conflicts in merge')

                    user = self.repo.default_signature
                    tree = self.repo.index.write_tree()
                    commit = self.repo.create_commit('HEAD',
                                                user,
                                                user,
                                                'Merge!',
                                                tree,
                                                [repo.head.target, remote_master_id])
                    # We need to do this or git CLI will think we are still merging.
                    self.repo.state_cleanup()
                    break
                else:
                    raise AssertionError('Unknown merge analysis result')

    def last_commit(self):
        commit = next(self.repo.walk(self.repo.head.target, pygit2.GIT_SORT_TOPOLOGICAL | pygit2.GIT_SORT_TIME))
        #commit = self.repo.revparse_single('HEAD')
        return Commit(str(commit.hex), commit.commit_time, commit.message,
                    Commiter(commit.author.name, commit.author.email)
                )


class SMTPNotify:
    def __init__(self, notify, key):
        self.host = notify.get('host')
        self.tls = notify.get('tls')
        self.user = notify.get('user')
        self.password = notify.get('password')
        self.fr = notify.get('from')
        self.to = notify.get('to')
        if self.host is None or self.fr is None or self.to is None or \
                (not self.user is None and self.password is None):
            logging.error("%s key not complete", key)
            raise ValueError()

    def __str__(self):
        s = "" if self.user is None else ", user = %s, password = %s" % (self.user, self.password)
        return "{ type = 'smtp', host = %s, from = %s, to = %s%s }" % (self.host, self.fr, self.to, s)

    def notify(self, name, status, log):
        server = smtplib.SMTP(self.host)
        if verbose:
            server.set_debuglevel(1)
        subj = "%s build %s" % (name, "DONE" if status else "FAIL")
        msg = "From: %s\r\nTo: %s\r\nSubject: %s\r\n\r\n%s" % (self.fr, self.to, subj, ''.join(log))
        if self.tls:
            server.starttls()
        if not self.user is None:
            server.login(self.user, self.password)
        server.sendmail(self.fr, self.to, msg)
        server.quit()


repo_types = { 'git': RepoGit }
notify_types = { 'smtp': SMTPNotify }


class Notify:
    @staticmethod
    def get(notify, key):
        if notify is None:
            return None

        ntype = notify.get('type')
        if ntype is None:
            logging.error("type in %s key not found", key)
            raise ValueError() 
        else:
            handler = notify_types.get(ntype)
            if handler is None:
                logging.error("notify type %s is unknown in %s key", ())
                raise ValueError()
            return handler(notify, key)


class Schedule:
    def __init__(self, schedule, key):
        if schedule is None:
            logging.error("%s key not found", key)
            raise ValueError()

        self.schedule = scheduler.Job.parse(schedule)
        self.sched_str = schedule

    def __str__(self):
            return self.sched_str 


class BuildConfig:
    def __init__(self, bconf, key):
        if bconf is None:
            logging.error("%s key not found", key)
            raise ValueError()

        self.state = None
        self.cmd = None
        self.run = None
        self.pkg = None
        self.publish = None
        self.log = None
        self.repodir = None
        self.vars = None

        self.notity = None

        self.set(bconf)

    def __str__(self):
        return "{\n  state = %s,\n  cmd = \"%s\",\n  run = %s,\n  log = %s,\n  repodir = %s,\n  notify = %s\n}" % (
                    self.state, 
                    self.cmd,
                    self.run,
                    self.log,
                    self.repodir,
                    self.notify
                )

    def set(self, bconf):
        error = False
        key = 'build'

        self.state = bconf.get('state')
        if self.state is None:
            logging.error("state in %s key not found", key)
            error = True

        self.cmd = bconf.get('cmd')
        if self.cmd is None:
            logging.error("cmd in %s key not found", key)
            error = True

        self.run = bconf.get('run')
        if self.run is None:
            logging.error("run in %s key not found", key)
            error = True
 
        self.pkg = bconf.get('pkg')
        if self.pkg is None:
            logging.error("pkg in %s key not found", key)
            error = True

        self.log = bconf.get('log')
        if self.log is None:
            logging.error("log in %s key not found", key)
            error = True

        self.lock = bconf.get('lock')
        if self.lock is None:
            logging.error("lock in %s key not found", key)
            error = True

        self.state = bconf.get('state')
        if self.state is None:
            logging.error("state in %s key not found", key)
            error = True


        self.repodir = bconf.get('repodir')
        if self.repodir is None:
            logging.error("repodir in %s key not found", key)
            error = True

        self.vars = bconf.get('vars')
        self.publish = bconf.get('publish')
        self.notify = Notify.get(bconf.get('notify'), 'notify')

        if error:
            raise ValueError()


class Build:
    def __init__(self, cfg):
        self.repo = None
        self.schedule = None
        self.buildconfig = None
        self.set(cfg)

    def __str__(self):
        return "repo = %s,\nschedule = %s,\nbuildconfig = %s" % (str(self.repo), str(self.schedule), str(self.buildconfig))

    def set(self, cfg):
        error = False
        try:
            self.repo = Repo.get(cfg.get('repo'), 'repo')	
        except ValueError:
            error = True

        try:
            self.buildconfig = BuildConfig(cfg.get('build'), 'buid')
        except ValueError:
            error = True

        try:
            self.schedule = Schedule(cfg.get('schedule'), 'schedule')
        except ValueError:
            error = True

        if error:
            raise ValueError("invalid config")
	
    def run(self):
        global running
        log = []
        f = open(self.buildconfig.lock, 'w')
        try:
            fcntl.flock(f.fileno(), fcntl.LOCK_EX | fcntl.LOCK_NB)
        except IOError as e:
            logging.info("another instance running")
            return (True, log)

        f.write(str(os.getpid()))
        d = datetime.datetime.now()
        self.repo.load(self.buildconfig.repodir)
        commit = self.repo.last_commit()
        if os.path.exists(self.buildconfig.state):
            with open(self.buildconfig.state, 'r') as f:
                prev_commit = f.read()
                if prev_commit == commit.commit:
                    logging.info("no changes from last publish")
                    return (True, log)

        logging.info("commit %s", str(commit))
        log.append("%s: commit %s" % (d.strftime("%Y-%m-%d %H:%M:%S"), str(commit)))
        if not running:
            logging.info("cancelled")
            log.append("cancelled")
            return (False, log)
        (exitcode, out, err) = proc_run(self.buildconfig.cmd, self.buildconfig.repodir, self.buildconfig.vars)
        for line in out:
            log.append(line)
        for line in err:
            log.append(line)
        if exitcode != 0:
            logging.error("run failed with code %d", exitcode)
            return (False, log)

        if not running:
            logging.info("cancelled")
            log.append("cancelled")
            return (False, log)

        log.append("\nRUN\n")
        (exitcode, out, err) = proc_run(self.buildconfig.run, self.buildconfig.repodir, self.buildconfig.vars)
        for line in out:
            log.append(line)
        for line in err:
            log.append(line)
        if exitcode != 0:
            logging.error("run failed with code %d", exitcode)
            return (False, log)

        if not running:
            logging.info("cancelled")
            log.append("cancelled")
            return (False, log)

        vars = {}
        vars.update(self.buildconfig.vars)
        vars['VERSION'] = "%s-git%s" % (vars['RELEASEVER'], commit.commit)
        vars['AUTHOR'] = "%s (%s)" % (commit.commiter.name, commit.commiter.email)
        vars['DESCRIPTION'] = ''.join(out)
        log.append("\nPackage %s by %s" % (vars['VERSION'], vars['AUTHOR']))
        log.append(vars['DESCRIPTION'])
        (exitcode, pkgout, pkgerr) = proc_run(self.buildconfig.pkg, self.buildconfig.repodir, vars)
        for line in pkgout:
            log.append(line)
            
        for line in pkgerr:
            log.append(line)

        if exitcode != 0:
            logging.error("package failed with code %d", exitcode)
            return (False, log)

        if not running:
            logging.info("cancelled")
            log.append("cancelled")
            return (False, log)

        if not self.buildconfig.publish is None:
            exists = False
            log.append("\nPUBLISH\n")
            (exitcode, pkgout, pkgerr) = proc_run(self.buildconfig.publish, self.buildconfig.repodir, vars)
            for line in pkgout:
                log.append(line)
                if line.find('already exists and cannot be modified'):
                    exists = True
            for line in pkgerr:
                log.append(line)

            if exitcode != 0:
                if exists:
                    msg = "publish not needed, already done"
                    logging.info(msg)
                else:
                    logging.error("publish failed with code %d", exitcode)
                    return (False, log)

        with open(self.buildconfig.state, 'w') as f:
            f.write(commit.commit)

        #for nupkg in glob.glob(os.path.join(self.buildconfig.repodir, "bin", "*", "*.nupkg")):
        #    os.remove(nupkg)
        
        f.close()
        return (True, log)


def build_check(*args):
    try:
        build = args[0]
        (status, log) = build.run()
        if len(log) > 0:
            try:
                with open(build.buildconfig.log, "w") as f:
                    for line in log:
                        f.write("%s\n" % line)
            except Exception as e:
                logging.error("log to %s failed: %s", build.buildconfig.log, str(e))
        if (len(log) > 0 or not status) and not build.buildconfig.notify is None:
            try:
                build.buildconfig.notify.notify(build.buildconfig.vars['NAME'], status, log)
            except Exception as e:
                logging.error("notify failed: %s", str(e))

    except ValueError:
        logging.error("invalid config %s", args.config)
    except:
        logging.error(traceback.format_exc())

def signal_handler(sig, frame):
        global running
        logging.info('signal %s, exiting' % str(sig))
        running = False


def main(args):
    global cfg
    global verbose
    global running

    verbose = args.verbose
    jobs = []
    f = open(args.config, 'r')
    cfg = yaml.load(f)
    f.close()
    try:
        build = Build(cfg)

        build.schedule.schedule.function = build_check
        build.schedule.schedule.args = (build, )

        jobs.append(build.schedule.schedule)
    except ValueError:
        logging.error("invalid config %s", args.config)
        sys.exit(1)

    if args.dry_run:
        sys.stdout.write("%s\n" % str(build))
        sys.exit(0)

    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    scheduler.start(jobs, daemon=True)

    while running:
        time.sleep(3)

    threads = scheduler.set_cancel()
    time.sleep(10)
    for t in threads:
        t.join(5)

    logging.info("exit")
    raise SystemExit


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Build from SCM repository')

    parser.add_argument('-c', '--config', dest='config', type=str, required=True,\
                         help='YAML config file')
    parser.add_argument('-d', '--dry-run', dest='dry_run', action='store_true', default=False,\
                         help='test run')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true', default=False,\
                         help='verbose')

    args = parser.parse_args() 
    main(args)

